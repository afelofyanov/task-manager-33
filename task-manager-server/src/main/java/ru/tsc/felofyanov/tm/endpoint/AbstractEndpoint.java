package ru.tsc.felofyanov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.service.IServiceLocator;
import ru.tsc.felofyanov.tm.dto.request.AbstractUserRequest;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.exception.system.AccessDeniedException;
import ru.tsc.felofyanov.tm.exception.system.PermissionException;
import ru.tsc.felofyanov.tm.model.User;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Getter
    @Nullable
    private IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(@Nullable AbstractUserRequest request, @Nullable Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();

        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final User user = getServiceLocator().getUserService().findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        @Nullable final Role roleUser = user.getRole();

        final boolean check = roleUser == role;
        if (!check) throw new PermissionException();
    }

    protected void check(AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
    }
}
