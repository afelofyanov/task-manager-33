package ru.tsc.felofyanov.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class DataSaveJsonJaxbResponse extends AbstractResponse {
}
