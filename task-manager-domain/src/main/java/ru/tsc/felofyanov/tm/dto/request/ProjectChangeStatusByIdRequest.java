package ru.tsc.felofyanov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.enumerated.Status;

@Getter
@Setter
public final class ProjectChangeStatusByIdRequest extends AbstractIdRequest {

    @Nullable
    private Status status;

    public ProjectChangeStatusByIdRequest(@Nullable String id, @Nullable Status status) {
        super(id);
        this.status = status;
    }
}
