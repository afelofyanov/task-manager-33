package ru.tsc.felofyanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

public interface IEndpoint {

    @NotNull
    String HOST = "localhost";

    @NotNull
    String PORT = "8080";

}
