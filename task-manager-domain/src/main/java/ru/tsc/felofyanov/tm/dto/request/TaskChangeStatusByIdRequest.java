package ru.tsc.felofyanov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.enumerated.Status;

@Getter
@Setter
public final class TaskChangeStatusByIdRequest extends AbstractIdRequest {

    @Nullable
    private Status status;

    public TaskChangeStatusByIdRequest(@Nullable String id, @Nullable Status status) {
        super(id);
        this.status = status;
    }
}
