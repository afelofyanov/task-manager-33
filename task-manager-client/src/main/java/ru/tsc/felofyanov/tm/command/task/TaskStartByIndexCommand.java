package ru.tsc.felofyanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.TaskChangeStatusByIndexRequest;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-start-by-index";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Start task by index.";
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");

        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(index, Status.IN_PROGRESS);
        getServiceLocator().getTaskEndpointClient().changeTaskStatusByIndex(request);
    }
}
