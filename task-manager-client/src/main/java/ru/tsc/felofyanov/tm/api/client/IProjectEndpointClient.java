package ru.tsc.felofyanov.tm.api.client;

import ru.tsc.felofyanov.tm.api.endpoint.IProjectEndpoint;

public interface IProjectEndpointClient extends IProjectEndpoint, IEndpointClient {
}
