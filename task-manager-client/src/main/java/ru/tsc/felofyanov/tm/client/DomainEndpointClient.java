package ru.tsc.felofyanov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.client.IDomainEndpointClient;
import ru.tsc.felofyanov.tm.dto.request.*;
import ru.tsc.felofyanov.tm.dto.response.*;

@NoArgsConstructor
public final class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpointClient {

    @NotNull
    @Override
    @SneakyThrows
    public DataLoadBackupResponse loadDataBackup(@NotNull DataLoadBackupRequest request) {
        return call(request, DataLoadBackupResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataLoadBase64Response loadDataBase64(@NotNull DataLoadBase64Request request) {
        return call(request, DataLoadBase64Response.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataLoadBinaryResponse loadDataBinary(@NotNull DataLoadBinaryRequest request) {
        return call(request, DataLoadBinaryResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataLoadJsonFasterXmlResponse loadDataJsonFasterXml(@NotNull DataLoadJsonFasterXmlRequest request) {
        return call(request, DataLoadJsonFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataLoadJsonJaxbResponse loadDataJsonJaxb(@NotNull DataLoadJsonJaxbRequest request) {
        return call(request, DataLoadJsonJaxbResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataLoadXmlFasterXmlResponse loadDataXmlFasterXml(@NotNull DataLoadXmlFasterXmlRequest request) {
        return call(request, DataLoadXmlFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataLoadXmlJaxbResponse loadDataXmlJaxb(@NotNull DataLoadXmlJaxbRequest request) {
        return call(request, DataLoadXmlJaxbResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataLoadYamlFasterXmlResponse loadDataYamlFasterXml(@NotNull DataLoadYamlFasterXmlRequest request) {
        return call(request, DataLoadYamlFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataSaveBackupResponse saveDataBackup(@NotNull DataSaveBackupRequest request) {
        return call(request, DataSaveBackupResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataSaveBase64Response saveDataBase64(@NotNull DataSaveBase64Request request) {
        return call(request, DataSaveBase64Response.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataSaveBinaryResponse saveDataBinary(@NotNull DataSaveBinaryRequest request) {
        return call(request, DataSaveBinaryResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataSaveJsonFasterXmlResponse saveDataJsonFasterXml(@NotNull DataSaveJsonFasterXmlRequest request) {
        return call(request, DataSaveJsonFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataSaveJsonJaxbResponse saveDataJsonJaxb(@NotNull DataSaveJsonJaxbRequest request) {
        return call(request, DataSaveJsonJaxbResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataSaveXmlFasterXmlResponse saveDataXmlFasterXml(@NotNull DataSaveXmlFasterXmlRequest request) {
        return call(request, DataSaveXmlFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataSaveXmlJaxbResponse saveDataXmlJaxb(@NotNull DataSaveXmlJaxbRequest request) {
        return call(request, DataSaveXmlJaxbResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataSaveYamlFasterXmlResponse saveDataYamlFasterXml(@NotNull DataSaveYamlFasterXmlRequest request) {
        return call(request, DataSaveYamlFasterXmlResponse.class);
    }
}
