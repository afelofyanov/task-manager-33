package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.client.*;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectTaskEndpointClient getProjectTaskEndpointClient();

    @NotNull
    ITaskEndpointClient getTaskEndpointClient();

    @NotNull
    IProjectEndpointClient getProjectEndpointClient();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IUserEndpointClient getUserEndpointClient();

    @NotNull
    IAuthEndpointClient getAuthEndpointClient();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainEndpointClient getDomainEndpointClient();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

    @NotNull
    ISystemEndpointClient getSystemEndpointClient();

}
