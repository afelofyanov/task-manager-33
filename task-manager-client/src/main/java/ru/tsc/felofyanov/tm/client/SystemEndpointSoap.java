package ru.tsc.felofyanov.tm.client;

import lombok.SneakyThrows;
import ru.tsc.felofyanov.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.felofyanov.tm.dto.request.ServerAboutRequest;

public class SystemEndpointSoap {

    @SneakyThrows
    public static void main(String[] args) {
        /*final String wsdl = "http://0.0.0.0:8080/SystemEndpoint?wsdl";
        ISystemEndpoint endpoint = Service.create(
                new URL(wsdl),
                new QName("http://endpoint.tm.felofyanov.tsc.ru/", "SystemEndpointService")
        ).getPort(ISystemEndpoint.class);*/
        System.out.println(ISystemEndpoint.newInstance().getAbout(new ServerAboutRequest()).getName());
    }
}
