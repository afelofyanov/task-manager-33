package ru.tsc.felofyanov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.client.IAuthEndpointClient;
import ru.tsc.felofyanov.tm.dto.request.UserLoginRequest;
import ru.tsc.felofyanov.tm.dto.request.UserLogoutRequest;
import ru.tsc.felofyanov.tm.dto.request.UserProfileRequest;
import ru.tsc.felofyanov.tm.dto.response.UserLoginResponse;
import ru.tsc.felofyanov.tm.dto.response.UserLogoutResponse;
import ru.tsc.felofyanov.tm.dto.response.UserProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        final AuthEndpointClient client = new AuthEndpointClient();
        client.connect();

        final AuthEndpointClient client1 = new AuthEndpointClient(client);

        System.out.println(client.profile(new UserProfileRequest()).getUser());

        System.out.println(client.login(new UserLoginRequest("test2", "test2")).getSuccess());
        System.out.println(client.login(new UserLoginRequest("test", "test")).getSuccess());

        System.out.println(client.profile(new UserProfileRequest()).getUser().getEmail());
        System.out.println(client1.profile(new UserProfileRequest()).getUser().getEmail());

        System.out.println(client.logout(new UserLogoutRequest()));
        System.out.println(client.profile(new UserProfileRequest()).getUser());

        client.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(@NotNull UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserProfileResponse profile(@NotNull UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }
}
