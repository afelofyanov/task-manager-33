package ru.tsc.felofyanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.ProjectGetByIndexRequest;
import ru.tsc.felofyanov.tm.dto.response.ProjectGetByIndexResponse;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show project by index.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();

        @NotNull final ProjectGetByIndexRequest projectGetByIndexRequest = new ProjectGetByIndexRequest(index);
        @NotNull final ProjectGetByIndexResponse projectGetByIndexResponse
                = getServiceLocator().getProjectEndpointClient().getProjectByIndex(projectGetByIndexRequest);
        @Nullable final Project project = projectGetByIndexResponse.getProject();
        showProject(project);
    }
}
