package ru.tsc.felofyanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.ProjectClearRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        @NotNull final ProjectClearRequest projectClearRequest = new ProjectClearRequest();
        getServiceLocator().getProjectEndpointClient().clearProject(projectClearRequest);
    }
}
