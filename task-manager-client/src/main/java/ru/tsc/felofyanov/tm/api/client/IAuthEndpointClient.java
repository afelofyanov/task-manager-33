package ru.tsc.felofyanov.tm.api.client;

import ru.tsc.felofyanov.tm.api.endpoint.IAuthEndpoint;

public interface IAuthEndpointClient extends IAuthEndpoint, IEndpointClient {
}
