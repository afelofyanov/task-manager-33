package ru.tsc.felofyanov.tm.api.client;

import ru.tsc.felofyanov.tm.api.endpoint.IProjectTaskEndpoint;

public interface IProjectTaskEndpointClient extends IProjectTaskEndpoint, IEndpointClient {
}
