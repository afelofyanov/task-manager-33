package ru.tsc.felofyanov.tm.api.client;

import ru.tsc.felofyanov.tm.api.endpoint.IDomainEndpoint;

public interface IDomainEndpointClient extends IDomainEndpoint, IEndpointClient {
}
