package ru.tsc.felofyanov.tm.api.client;

import ru.tsc.felofyanov.tm.api.endpoint.IUserEndpoint;

public interface IUserEndpointClient extends IUserEndpoint, IEndpointClient {
}
